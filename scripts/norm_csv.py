#!/usr/bin/env python3
"""
Norm CSV values:
- strip cells text content (removing duplicated spaces, tabs, CR, ...)
- taking columns in account before empty column
- skip rows if empty content in one of those columns: adresse, horaires, services et aide
"""
import argparse
import csv
import itertools
import logging
import re
import sys
from pathlib import Path
from typing import Dict

import yaml

NORM_SPACES_RE = re.compile(r"\s+")

log = logging.getLogger(__name__)


def normalize_values(source_file: Path, target_file: Path):
    with source_file.open("rt", encoding="utf-8") as fdin:
        reader = csv.reader(fdin)
        with target_file.open("wt", encoding="utf-8") as fdout:
            writer = csv.writer(fdout, quoting=csv.QUOTE_ALL)
            is_header = True
            columns_count = None
            for row in reader:
                # Don't take in account columns after empty named column
                if is_header:
                    is_header = False
                    columns_count = row.index("") if "" in row else len(row)
                    mandatory_column_ids = [
                        row.index(col_name)
                        for col_name in ("adresse", "horaires", "services", "aide")
                    ]

                # Normalize spaces in content of selected columns
                norm_row = list(map(str.strip, row))[:columns_count]

                # Skip row if missing value in mandatory columns
                if any(norm_row[col_id] == "" for col_id in mandatory_column_ids):
                    continue

                writer.writerow(norm_row)


def main():
    parser = argparse.ArgumentParser(description="Strip cell values")
    parser.add_argument("source_file", type=Path, help="CSV source file")
    parser.add_argument("target_file", type=Path, help="CSV target file")
    args = parser.parse_args()

    # Check csv file existence
    if not args.source_file.exists():
        parser.error("CSV file [%s] not found", args.source_file)
    source_file = args.source_file

    # Normalize_values
    normalize_values(source_file, args.target_file)


if __name__ == "__main__":
    sys.exit(main())
