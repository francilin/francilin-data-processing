#!/usr/bin/env python3
"""
Generate structures GeoJSON file from geolocated structures CSV file.
"""
import argparse
import csv
import logging
import sys
from pathlib import Path
from typing import Dict

import ujson as json

log = logging.getLogger(__name__)


def iter_structure_info_from_csv(csv_file: Path):
    """Iterate on structure information."""

    def filter_empty_values(info_dict: Dict[str, str]) -> Dict:
        """Remove keys from given dict if matching values are empty."""
        return {key: value for key, value in info_dict.items() if value}

    with csv_file.open("rt", encoding="utf-8") as fdin:
        reader = csv.DictReader(fdin)

        for row_dict in reader:

            # Skip empty data
            if row_dict["lieu_nom"] == "":
                continue

            # Skip structure without geolocation
            if not row_dict["latitude"]:
                log.warning(
                    "No location found for [%s] %s - %s - %s",
                    row_dict["lieu_nom"],
                    row_dict["adresse"],
                    row_dict["code_postal"],
                    row_dict["commune"],
                )
                continue

            # Get aide list
            aide_list = list(
                filter(
                    lambda val: val.strip() != "",
                    map(str.strip, row_dict["aide"].split(",")),
                )
            )

            # Get service list
            service_list = list(
                filter(
                    lambda val: (val.strip() != "") and len(val) == 3,
                    map(str.strip, row_dict["services"].split(",")),
                )
            )

            label_list = list(
                filter(
                    lambda val: val.strip() != "",
                    map(str.strip, row_dict["label"].split(",")),
                )
            )

            # Compute structure info
            yield filter_empty_values(
                {**row_dict, "aide": aide_list, "services": service_list, "label": label_list}
            )


def convert_structure_info_to_geofeature(structure_info: Dict[str, str]):
    lon = float(structure_info["longitude"])
    lat = float(structure_info["latitude"])
    return {
        "type": "Feature",
        "geometry": {"type": "Point", "coordinates": (lon, lat)},
        "properties": {
            key: value
            for key, value in structure_info.items()
            if key not in {"longitude", "latitude"}
        },
    }


def convert_to_geojson(
    source_file: Path, target_file: Path,
):
    """Convert structures CSV file to structures GeoJSON file."""

    structures_geo_info = {
        "type": "FeatureCollection",
        "features": [
            convert_structure_info_to_geofeature(struct_info)
            for struct_info in iter_structure_info_from_csv(source_file)
        ],
    }
    with target_file.open("wt", encoding="utf-8") as fdout:
        json.dump(
            structures_geo_info, fdout, ensure_ascii=False, sort_keys=True, indent=2
        )


def main():
    parser = argparse.ArgumentParser(description="Generate single GeoJSON")
    parser.add_argument("source_file", type=Path, help="CSV source file")
    parser.add_argument("target_file", type=Path, help="GeoJSON target file")
    args = parser.parse_args()

    # Check source file existence
    if not args.source_file.exists():
        parser.error("CSV file [%s] not found", args.source_file)
    source_file = args.source_file

    # Checks target dir existence
    target_dir = args.target_file.parent
    if not target_dir.exists():
        parser.error("Target dir [%s] not found", target_dir)
    target_file = args.target_file

    # Let's convert!
    convert_to_geojson(source_file, target_file)


if __name__ == "__main__":
    sys.exit(main())
