#!/bin/bash
#
# Process structures CSV file into GeoJSON file
#
# Pierre Dittgen, Jailbreak
#

TMP_DIR=$(mktemp --directory --suffix=.francilin)
CURRENT_PATH=$(dirname $PWD/$0)

norm_csv() {
    source_csv=$1
    target_csv=$2

    python3 $CURRENT_PATH/scripts/norm_csv.py $source_csv $target_csv
}


add_geo_coords() {
    source_csv=$1
    target_csv=$2

    geo_2=$TMP_DIR/geo_2.csv
    geo_3=$TMP_DIR/geo_3.csv
    geo_4=$TMP_DIR/geo_4.csv

    FRANCILIN_ID_COL="francilin_id"

    # Then extract wanted columns
    xsv select "\"$FRANCILIN_ID_COL\",\"adresse\",\"commune\",\"code_postal\"" $source_csv > $geo_2

    # Then call geo api
    if [ ! -e $geo_3 ]; then
        curl -X POST -F data=@$geo_2 -F columns=adresse -F columns=commune -F postcode=code_postal https://api-adresse.data.gouv.fr/search/csv/ > $geo_3
    fi

    # Only keep $FRANCILIN_ID_COL and latitude, longitude
    xsv select "\"$FRANCILIN_ID_COL\",\"latitude\",\"longitude\"" $geo_3 > $geo_4

    # Join files
    xsv join $FRANCILIN_ID_COL $source_csv $FRANCILIN_ID_COL $geo_4 > $target_csv
}

convert_to_geojson() {
    source_csv=$1
    target_geojson=$2

    python3 $CURRENT_PATH/scripts/generate_jsons.py $source_csv $target_geojson
}


#### main ####

if [ $# -ne 2 ]; then
    echo "usage: $0 <source_fichier_normalise.csv> <structures.geojson>"
    exit 1
fi

SOURCE_CSV="$1"
TARGET_GEOJSON="$2"

if [ "$SOURCE_CSV" == "" -o ! -e "$SOURCE_CSV" ]; then
    echo "Source CSV file not found: $SOURCE_CSV"
    exit 1
fi

if [ "$TARGET_GEOJSON" == "" ]; then
    echo "Undefined target GeoJSON file"
    exit 1
fi

mkdir -p $TMP_DIR
norm_csv $SOURCE_CSV $TMP_DIR/temp_1.csv
add_geo_coords $TMP_DIR/temp_1.csv $TMP_DIR/temp_2.csv
convert_to_geojson $TMP_DIR/temp_2.csv $TARGET_GEOJSON
rm -fR $TMP_DIR