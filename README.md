# Traitement de données Francil'IN

Transformation des données concernant les lieux d'inclusion numérique
d'Île de France :

- géolocalisation automatique
- transformation de la source CSV en JSON

## Dépendances techniques

- bash
- python3 (virtual env)
- xsv
- curl

## Données

La liste des lieux d'inclusion numérique d'Île de France est disponible dans un tableur nommé `Fichiers Draft Carto IN IdF` au sein d'un synology avec accès authentifié.  
Ce tableur est maintenu par l'équipe Francil'IN, il comprend un onglet
`SOURCE - Fichier normalisé` contenant 1 lieu par ligne.

Pour les besoins du projet, le fichier des lieux d'inclusion est copié à intervalle régulier dans le dossier `src` (sous le nom `source_fichier_normalise.csv`) du projet [francilin-data](https://gitlab.com/francilin/francilin-data/). Le dossier `dist` de ce même projet contient le fichier `structures.geojson` utilisé par le projet [francilin-carto](https://gitlab.com/francilin/francilin-carto)

## Mise à jour des données

La première fois, cloner le projet [francilin-data](https://gitlab.com/francilin/francilin-data/) au même niveau que francilin-data-processing :

```bash
git clone git@gitlab.com:francilin/francilin-data.git
```

### Export des données d'inclusion numérique

La procédure d'export est (actuellement) manuelle :

- se connecter sur l'interface synology
- ouvrir le tableur
- exporter le contenu de l'onglet `SOURCE - Fichier normalisé` en CSV et l'enregistrer sous le nom `source_fichier_normalise.csv` dans le dossier `src` du dépôt francilin-data.

### Génération du fichier GeoJSON

```bash
./process_structures.sh ../francilin-data/source/source_fichier_normalise.csv ../francilin-data/dist/structures.geojson
```
